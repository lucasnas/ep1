#ifndef ELEITOR_HPP
#define ELEITOR_HPP
#include <string>
#include <iostream>
#include <fstream>
#include "pessoa.hpp"

using namespace std;

class Eleitor : public Pessoa{

private:
  int NR_TITULO_ELEITOR;
  string DATA_NASCIMENTO;
  //string NM_PESSOA;
  //int NR_CPF_PESSOA;
  //string NM_PESSOA;

public:
  Eleitor();
  ~Eleitor();
  int get_NR_TITULO_ELEITOR();
  void set_NR_TITULO_ELEITOR(int NR_TITULO_ELEITOR);
  string get_DATA_NASCIMENTO();
  void set_DATA_NASCIMENTO(string DATA_NASCIMENTO);
};

#endif
