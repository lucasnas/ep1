#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

class Candidato{

private:
  int numeroDoCandidato;
  int voteSenador;
  string votarUrna;
  bool votarNulo;
  string decisaoPessoa;


private:
  string NR_PARTIDO;
  string SG_PARTIDO;
  string NM_URNA_CANDIDATO;
  string NM_PARTIDO;
  string DS_CARGO;
  string NM_CANDIDATO;
  string NR_CANDIDATO;

//string NM_PESSOA;
//int NR_CPF_PESSOA;
//string NM_PESSOA;

public:
  Candidato();
  ~Candidato();
  string get_NR_PARTIDO();
  void set_NR_PARTIDO(string NR_PARTIDO);
  string get_NM_URNA_CANDIDATO();
  void set_NM_URNA_CANDIDATO(string NM_URNA_CANDIDATO);
  string get_NM_PARTIDO();
  void set_NM_PARTIDO(string NM_PARTIDO);
  string get_DS_CARGO();
  void set_DS_CARGO(string DS_CARGO);
  string get_NM_CANDIDATO();
  void set_NM_CANDIDATO(string NM_CANDIDATO);
  string get_NR_CANDIDATO();
  void set_NR_CANDIDATO(string NR_CANDIDATO);
  string get_SG_PARTIDO();
  void set_SG_PARTIDO(string SG_PARTIDO);

  void votarCandidatoDF();
  void votarDeputadoDistrital();
  void votarSenador(int postoSenador);
  void votarPresidente();
  int verificacaoDoVoto();
  void votarGovernador();
  void printCandidatos(string DS_CARGO,string cargo, int numeroDoCandidato, bool votar, int posto);
  void printCandidatos(string DS_CARGO,string cargo, int numeroDoCandidato);
  void decisao(string DS_CARGO, int posto);



};

#endif
