#ifndef PESSOA_HPP
#define PESSOA_HPP
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

class Pessoa{

private:
  string NM_PESSOA;
  int NR_CPF_PESSOA;

public:
  Pessoa();
  ~Pessoa();
  string get_NM_PESSOA();
  void set_NM_PESSOA(string NM_PESSOA);
  int get_NR_CPF_PESSOA();
  void set_NR_CPF_PESSOA(int NR_CPF_PESSOA);
};

#endif
