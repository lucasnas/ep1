#include "pessoa.hpp"

using namespace std;

Pessoa::Pessoa(){

}

Pessoa::~Pessoa(){

}

string Pessoa::get_NM_PESSOA(){
  return NM_PESSOA;
}

void Pessoa::set_NM_PESSOA( string NM_PESSOA){
  this->NM_PESSOA=NM_PESSOA;
}

int Pessoa::get_NR_CPF_PESSOA(){
  return NR_CPF_PESSOA;
}

void Pessoa::set_NR_CPF_PESSOA(int NR_CPF_PESSOA){
  this->NR_CPF_PESSOA = NR_CPF_PESSOA;
}
