#include "eleitor.hpp"
#include "candidato.hpp"
#define BR "Brasil"
#define DF "Distrito Federal"

Candidato::Candidato(){

}

Candidato::~Candidato(){

}


string Candidato::get_NR_PARTIDO(){
  return NR_PARTIDO;
}

void Candidato::set_NR_PARTIDO(string NR_PARTIDO){
  this->NR_PARTIDO=NR_PARTIDO;
}

string Candidato::get_DS_CARGO(){
  return DS_CARGO;
}

void Candidato::set_DS_CARGO(string DS_CARGO){
  this -> DS_CARGO = DS_CARGO;
}

string Candidato::get_NM_PARTIDO(){
  return NM_PARTIDO;
}

void Candidato::set_NM_PARTIDO(string NM_PARTIDO){
  this -> NM_PARTIDO = NM_PARTIDO;
}

string Candidato::get_NM_URNA_CANDIDATO(){
  return NM_URNA_CANDIDATO;
}

void Candidato::set_NM_URNA_CANDIDATO(string NM_URNA_CANDIDATO){
  this -> NM_URNA_CANDIDATO = NM_URNA_CANDIDATO;
}

string Candidato::get_NR_CANDIDATO(){
  return NR_CANDIDATO;
}

void Candidato::set_NR_CANDIDATO(string NR_CANDIDATO){
  this->NR_CANDIDATO = NR_CANDIDATO;
}

string Candidato::get_NM_CANDIDATO(){
  return NM_CANDIDATO;
}

void Candidato::set_NM_CANDIDATO(string NM_CANDIDATO){
  this -> NM_CANDIDATO = NM_CANDIDATO;
}

string Candidato::get_SG_PARTIDO(){
  return SG_PARTIDO;
}

void Candidato::set_SG_PARTIDO(string SG_PARTIDO){
  this -> SG_PARTIDO = SG_PARTIDO;
}


int Candidato::verificacaoDoVoto(){

    while(true){

        cin >> votarUrna;

        if(votarUrna.compare("0")==0){
            cout << "Erro de entranda nula" << "\n";
            return 0;
        }

        else {
            try
            {
                return stoi(votarUrna,nullptr,10);
            }

            catch(exception &e)
            {
                cout<<"Tente de novo!" <<"\n";
            }
        }
    }
}

void Candidato::decisao(string DS_CARGO, int posto) {
    while(1) {
        string decisaoPessoa;
        cout << "(1) confirmar voto" << endl;
        cout << "(2) corrigir voto" <<endl;
        cout << "Escolha: ";
        cin >> decisaoPessoa;

        if(decisaoPessoa.compare("2")==0)
        {

            if(DS_CARGO.compare("DEPUTADO DISTRITAL")==0)
            {
                this -> votarCandidatoDF();
                break;
            }

            if(DS_CARGO.compare("DEPUTADO FEDERAL")==0)
            {
                this -> votarCandidatoDF();
                break;
            }

            if(DS_CARGO.compare("SEGUNDO SUPLENTE")==0&&posto==1)
            {
                this -> votarSenador(posto);
                break;
            }
            if(DS_CARGO.compare("SEGUNDO SUPLENTE")==0&&posto==2)
            {
                this -> votarSenador(posto);
                break;
            }

            if(DS_CARGO.compare("VICE-GOVERNADOR") == 0) {
                this -> votarGovernador();
                break;
            }

            if(DS_CARGO.compare("VICE-PRESIDENTE") == 0) {
                this -> votarPresidente();
                break;
            }
        }
        else if(decisaoPessoa.compare("1") == 0) {
            cout<<"Voto confirmado!"<<endl;break;
        }
        else {
            cout<<"Digitar de novo!"<<endl;
        }
    }
}

void Candidato::printCandidatos(string DS_CARGO, string cargo, int numeroDoCandidato)
{

    ifstream arquivoBR("consulta_cand_2018_BR.csv");

    if(!arquivoBR.good())
    cout << "File not found!" << endl;

  if(DS_CARGO.compare(DF) == 0)
  {
    ifstream arquivoDF("consulta_cand_2018_DF.csv");
    arquivoBR.swap(arquivoDF);
  }

  votarNulo = true;

  while(arquivoBR.good())
  {

    getline(arquivoBR, DS_CARGO, ';');
    getline(arquivoBR, NR_CANDIDATO, ';');
    getline(arquivoBR, NM_CANDIDATO, ';');
    getline(arquivoBR, NM_URNA_CANDIDATO, ';');
    getline(arquivoBR, NR_PARTIDO, ';');
    getline(arquivoBR, SG_PARTIDO, ';');
    getline(arquivoBR, NM_PARTIDO);

  if(stoi(get_NR_CANDIDATO(), nullptr, 10) == numeroDoCandidato && get_DS_CARGO().compare(cargo) == 0) {

    cout<<"Cargo: "<<get_DS_CARGO()<<endl;
    cout<<"Número do candidato: "<< get_NR_CANDIDATO()<<endl;
    cout<<"Nome do candidato: "<< get_NM_CANDIDATO()<<endl;
    cout<<"Nome na urna: " <<get_NM_URNA_CANDIDATO()<<endl;
    cout<<"Número do Partido: "<< get_NR_PARTIDO()<<endl;
    cout<<"Sigla do Partido: "<< get_SG_PARTIDO()<<endl;
    cout<<"Nome do Partido: "<<get_NM_PARTIDO()<<endl;
    arquivoBR.close();
    votarNulo=false;
    this -> decisao(cargo,0);

    return;
    }
}

if(votarNulo)
{
    cout<<"Voto nulo!"<<endl;
    this->decisao(cargo,0);

    return;
}
}


void Candidato::printCandidatos(string DS_CARGO, string cargo, int numeroDoCandidato,bool votar,int posto)
{

    ifstream arquivoBR("consulta_cand_2018_BR.csv");


    if(!arquivoBR.good())
        cout << "File not found" <<endl;

    if(DS_CARGO.compare(DF) == 0)
    {
        ifstream arquivoDF("consulta_cand_2018_DF.csv");
        arquivoBR.swap(arquivoDF);
    }

  votarNulo = true;

  while(arquivoBR.good())
  {
    getline(arquivoBR, DS_CARGO, ';');
    getline(arquivoBR, NR_CANDIDATO, ';');
    getline(arquivoBR, NM_CANDIDATO, ';');
    getline(arquivoBR, NM_URNA_CANDIDATO, ';');
    getline(arquivoBR, NR_PARTIDO, ';');
    getline(arquivoBR, SG_PARTIDO, ';');
    getline(arquivoBR, NM_PARTIDO);

  if(stoi(get_NR_CANDIDATO(), nullptr, 10) == numeroDoCandidato && get_DS_CARGO().compare(cargo) == 0)
  {

    cout<<"Cargo: " << get_DS_CARGO()<<endl;
    cout<<"Número do candidato: "<<get_NR_CANDIDATO()<<endl;
    cout<<"Nome do candidato: "<< get_NM_CANDIDATO()<<endl;
    cout<<"Nome na urna: "<<get_NM_URNA_CANDIDATO()<<endl;
    cout<<"Número do Partido: "<<get_NR_PARTIDO()<<endl;
    cout<<"Sigla do Partido: "<<get_SG_PARTIDO()<<endl;
    cout<<"Nome do Partido: "<<get_NM_PARTIDO()<< endl;
    arquivoBR.close();

    votarNulo = false;

    if(votar)
    {
        this -> decisao(cargo, posto);
    }
    return;
  }

}

if(votarNulo)
{
    cout<<"Voto nulo!"<<endl;
        if(votar)
        {
            this -> decisao(cargo, posto);
        }
        return;
}

}

void Candidato::votarPresidente()
{
    cout << "                 PRESIDENTE" <<endl;
    cout << "Numero para presidente: " <<endl;
    cout << "(1) Votar em Branco)" <<endl;

    numeroDoCandidato = this -> verificacaoDoVoto();

    if(numeroDoCandidato != 1)
    {
        this -> printCandidatos(BR, "PRESIDENTE", numeroDoCandidato, false, 1);
        cout << "VICE-PRESIDENTE" <<endl;
        this -> printCandidatos(BR, "VICE-PRESIDENTE", numeroDoCandidato, true, 1);
    }
    if(numeroDoCandidato == 1)
    {
        cout << "Branco!" <<endl;
        this -> decisao("VICE-PRESIDENTE", 1);
    }

}

void Candidato::votarGovernador()
{
    cout << "                   GOVERNADOR" <<endl;
    cout << "Numero para governador: "<<endl;
    cout << "(1) Votar em Branco)" <<endl;
    numeroDoCandidato = this -> verificacaoDoVoto();

    if(numeroDoCandidato != 1)
    {
        this -> printCandidatos(DF, "GOVERNADOR", numeroDoCandidato, false, 1);
        cout << "VICE-GOVERNADOR" << endl;
        this -> printCandidatos(DF, "VICE-GOVERNADOR", numeroDoCandidato, true, 1);
    }
    if(numeroDoCandidato == 1) {
        cout << "Voto em branco!" << endl;
        this -> decisao("VICE-GOVERNADOR", 1);
    }
}


void Candidato::votarSenador(int postoSenador) {
    if(postoSenador == 1) {
        cout << "            SENADOR(Primeira vaga)" << endl;
        cout << "Numero do senador: " << endl;
        cout << "(0) Votar em Branco):" << endl;

        numeroDoCandidato = this -> verificacaoDoVoto();
        voteSenador = numeroDoCandidato;

        if(numeroDoCandidato != 0)
        {
            this -> printCandidatos(DF, "SENADOR", numeroDoCandidato, false, 1);
            cout << "Primeiro suplente: " << endl;
            this -> printCandidatos(DF, "PRIMEIRO SUPLENTE", numeroDoCandidato, false, 1);
            cout << "Segundo suplente: " << endl;
            this -> printCandidatos(DF, "SEGUNDO SUPLENTE", numeroDoCandidato, true, 1);
        }

        if(numeroDoCandidato == 0)
        {
            cout << "Voto em branco!" << endl;
            this -> decisao("SEGUNDO SUPLENTE", 1);
        }
    }
    else if(postoSenador == 2) {
        cout << "              SENADOR - segunda vaga" << endl;
        cout << "numero do senador:" << endl;
        cout << "(0) Votar em Branco" << endl;

        numeroDoCandidato = this -> verificacaoDoVoto();

        if(numeroDoCandidato!=0)
        {
          if(voteSenador == numeroDoCandidato) {
               numeroDoCandidato = 1111;
           }

            this->printCandidatos(DF,"SENADOR",numeroDoCandidato, false, 2);
            cout<<"Primeiro suplente:"<<endl;
            this -> printCandidatos(DF, "PRIMEIRO SUPLENTE", numeroDoCandidato, false, 2);
            cout << "Segundo suplente: " << endl;
            this -> printCandidatos(DF, "SEGUNDO SUPLENTE", numeroDoCandidato, true, 2);
        }
        if(numeroDoCandidato == 0)
        {
            cout << "Voto em branco!" << endl;
            this -> decisao("SEGUNDO SUPLENTE", 2);
        }
    }
}

void Candidato::votarCandidatoDF()
{
    cout << "               DEPUTADO FEDERAL" << endl;
    cout << "Numero para deputado federal" << endl;
    cout << "(0) Votar em Branco)" << endl;
    numeroDoCandidato = this -> verificacaoDoVoto();

    if(numeroDoCandidato != 0)
    {
        this -> printCandidatos(DF, "DEPUTADO FEDERAL", numeroDoCandidato);
    }

    if(numeroDoCandidato==0)
    {
        cout << "Voto em branco!" << endl;
        this -> decisao("DEPUTADO FEDERAL", 0);
    }

}

void Candidato::votarDeputadoDistrital()
{

    cout << "              DEPUTADO DISTRITAL" << endl;
    cout << "Digite o número para deputado distrital(5 dígitos): " << endl;
    cout << "(0) Votar em Branco)" << endl;
    numeroDoCandidato = this -> verificacaoDoVoto();

    if(numeroDoCandidato != 0)
    {
        this -> printCandidatos(DF, "DEPUTADO DISTRITAL", numeroDoCandidato);
    }
    if(numeroDoCandidato == 0)
    {
        cout << "Voto em branco!" << endl;
        this -> decisao("DEPUTADO DISTRITAL", 0);
    }

}
