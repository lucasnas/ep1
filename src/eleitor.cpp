#include "eleitor.hpp"
#include "pessoa.hpp"
#include "candidato.hpp"

using namespace std;

Eleitor::Eleitor(){
}

Eleitor::~Eleitor(){

}

int Eleitor::get_NR_TITULO_ELEITOR()
{
  return NR_TITULO_ELEITOR;
}

void Eleitor::set_NR_TITULO_ELEITOR(int NR_TITULO_ELEITOR)
{
  this->NR_TITULO_ELEITOR = NR_TITULO_ELEITOR;
}

string Eleitor::get_DATA_NASCIMENTO(){
  return DATA_NASCIMENTO;
}

void Eleitor::set_DATA_NASCIMENTO(string DATA_NASCIMENTO){
  this->DATA_NASCIMENTO=DATA_NASCIMENTO;
}
